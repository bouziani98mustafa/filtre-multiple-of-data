import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../models/user.model';

@Component({
  selector: 'app-user-profil',
  templateUrl: './user-profil.component.html',
  styleUrls: ['./user-profil.component.css']
})
export class UserProfilComponent implements OnInit {

  user: User;

  constructor(private route: Router) { }

  ngOnInit(): void {
    this.user = history.state;
  }

  backToList(): void {
    this.route.navigate(['']);
  }

}
