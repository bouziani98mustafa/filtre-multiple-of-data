import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {ContentComponent} from './content/content.component';
import {UserProfilComponent} from './user-profil/user-profil.component';
import {UsersListComponent} from './users-list/users-list.component';
import {UserRowComponent} from './user-row/user-row.component';
import {UserSearchComponent} from './user-search/user-search.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {DataImportService} from './services/data-import.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ContentComponent,
    UserProfilComponent,
    UsersListComponent,
    UserRowComponent,
    UserSearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    DataImportService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
