import {Component, Input, OnInit} from '@angular/core';
import {User} from '../models/user.model';

@Component({
  selector: '[app-user-row]',
  templateUrl: './user-row.component.html',
  styleUrls: ['../users-list/users-list.component.css']
})
export class UserRowComponent implements OnInit {

  @Input() user: User;

  constructor() { }

  ngOnInit(): void {
  }

}
