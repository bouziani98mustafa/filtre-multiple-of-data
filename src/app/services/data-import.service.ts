import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user.model';
import {Observable} from 'rxjs';

//Decorator
@Injectable({
  providedIn: 'root'
})
export class DataImportService {

  url = 'https://jsonplaceholder.typicode.com/users';
  users: User[];

  constructor(private http?: HttpClient) { }

  dataUsers(): Observable<any> {
    return this.http.get<any>(this.url);
  }
  deleteUsers(id: number): Observable<any> {
    return this.http.delete<any>(`${this.url}/${id}`);
  }
  updateUser(id: number, value: any): Observable<any> {
    return this.http.put(`${this.url}/${id}`, value);
  }
  getUser(): Observable<any> {
    return this.http.get(`${this.url}`);
  }
}
