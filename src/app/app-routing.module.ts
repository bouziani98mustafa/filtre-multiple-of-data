import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UserProfilComponent} from './user-profil/user-profil.component';
import {ContentComponent} from './content/content.component';

const routes: Routes = [
  {path: '', component: ContentComponent},
  {path: 'details', component: UserProfilComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
