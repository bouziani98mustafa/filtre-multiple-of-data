export class Address {
  suite: string;
  street: string;
  city: string;
  zipecode: string;

  constructor(suite: string, street: string, city: string, zipecode: string) {
    this.suite = suite;
    this.street = street;
    this.city = city;
    this.zipecode = zipecode;
  }
}
