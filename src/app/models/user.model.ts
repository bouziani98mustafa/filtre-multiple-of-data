import {Company} from './company.model';
import {Address} from './address.model';

export class User {
  name: string;
  email: string;
  phone: string;
  website: string;
  company: Company;
  address: Address;

  constructor(name: string, email: string, phone: string, website: string, company: Company, address: Address) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.website = website;
    this.company = company;
    this.address = address;
  }
}
