import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {DataImportService} from '../services/data-import.service';
import {User} from '../models/user.model';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit, OnChanges {

  headers: string[] = ['Name', 'Email', 'Phone', 'Company Name'];
  users: any;
  @Input() name: string;
  @Input() email: string;
  @Input() names: string;
  userss: Observable<User[]>;

  constructor(private route: Router, private usersService: DataImportService) {
    this.users = this.usersService.users;
  }
  reloadData() {
    this.userss = this.usersService.dataUsers();
  }
  ngOnInit(): void {
    console.log(this.users);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.showUsers();
  }

  showUsers(): void {
    console.log(this.names);
    if ((this.name !== undefined) && (this.email !== undefined) && (this.names !== undefined)) {
      const usersFound: User[] = new Array();
      for (const user of this.usersService.users) {
        if ((user.email.toLowerCase()).startsWith(this.email)){
          if((user.name.toLowerCase()).startsWith(this.name.toLowerCase())) {
          if ((this.names === 'fnamelname') && (user.name.split(' ').length === 2)) {
            usersFound.push(user);
          } else if ((this.names === 'fnamemnamelname') && (user.name.split(' ').length === 3)) {
            usersFound.push(user);
          }
        }
      }
      this.users = usersFound;
    }} else if ((this.name !== undefined) && (this.email === undefined)) {
      const usersFound: User[] = new Array();
      for (const user of this.usersService.users) {
        if ((user.name).startsWith(this.name)) {
          usersFound.push(user);
        }
      }
      this.users = usersFound;
    } else if ((this.name === undefined) && (this.email !== undefined)) {
      const usersFound: User[] = new Array();
      for (const user of this.usersService.users) {
        if ((user.email).startsWith(this.email)) {
          usersFound.push(user);
        }
      }
      this.users = usersFound;
    } else {
      this.usersService.dataUsers().subscribe({
        next: users => {
          this.usersService.users = users;
          this.users = users;
        }
      });

    }
  }
  deleteUser(id: number) {
    this.usersService.deleteUsers(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
  userDetails(id: number){
    this.route.navigate(['details', id]);
  }

  updateUser(id: number){
    this.route.navigate(['update', id]);
  }

  showProfil(user): void {
    this.route.navigate(['details'], {state: user});
  }

}
